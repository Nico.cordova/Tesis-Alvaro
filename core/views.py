# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render_to_response, render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import render

def user_login(request):
    logout(request)
    template = "login.html"
    user,password = "", ""
    if request.POST:
        username = request.POST["Username"]
        password = request.POST["Password"]
        user = authenticate(username=username, password=password)
        if user != None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse_lazy('index'))

    return render(request, template, {})


@login_required(login_url='/')
def index(request):
    data = {}
    template_name = 'index.html'
    return render(request, template_name, data)

@login_required(login_url='/')
def approved(request):
    data = {}
    template_name = 'Avance/ramos.html'
    return render(request, template_name, data)
